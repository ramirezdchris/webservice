<?php

class Database
{
    private $host = 'localhost';
    private $db = 'clinicautec';
    private $username = 'root';
    private $password = 'root';
    private $conn;

    // Conexion a Base de Datos
    public function connect()
    {
        $this->conn = null;

        try {
            $this->conn = new PDO(
                'mysql:host=' . $this->host . ';dbname = ' . $this->db,
                $this->username,
                $this->password
            );
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo 'Se conecto';
        } catch (PDOException $e) {
            //echo 'Connection Error: ' . $e->getMessage();
        }
        return $this->conn;
    }
}