<?php
class Personal{

    private $conn;
    private $table = 'clinicautec.personal';

    public $id_personal;
    public $nombre;
    public $apellido;
    public $dui;
    public $correo;
    public $telefono;

    public function __construct($db){
        $this->conn = $db;
    }
    
    public function read(){
        $query = 'SELECT * FROM '. $this->table ;
        //Preparar la consulta
        $stmt = $this->conn->prepare($query);
        //Ejecutar la consulta
        $stmt->execute();
        
        return $stmt;
    }
    
    public function  read_single(){
        //QUERY
        $query = 'SELECT * FROM '.$this->table. ' WHERE id_personal = ?';
        
        //PREPARAR LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //VERIFICAMOS EL ID 
        $stmt->bindParam(1, $this->id);
        
        //Ejecutamos la consulta
        $stmt->execute();
        
        //Obtenemos en una variable $row toda la fila de la tabla
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        //SETIAMOS TODAS LA VARIABLES PARA LLENAR LA FILA
        $this->id_personal = $row['id_personal'];
        $this->nombre = $row['nombre'];
        $this->apellido = $row['apellido'];
        $this->dui = $row['dui'];
        $this->correo = $row['correo'];
        $this->telefono = $row['telefono'];
        
    }
    
    public function create(){
        //QUERY
        $query ='INSERT INTO '. $this->table . ' VALUES(0,:nombre,:apellido,:dui,:correo,:telefono);';
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        
        //LIMPIAMOS LOS DATOS
        $this->nombre = htmlspecialchars(strip_tags($this->nombre));
        $this->apellido = htmlspecialchars(strip_tags($this->apellido));
        $this->dui = htmlspecialchars(strip_tags($this->dui));
        $this->correo = htmlspecialchars(strip_tags($this->correo));
        $this->telefono = htmlspecialchars(strip_tags($this->telefono));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':nombre', $this->nombre);
        $stmt->bindParam(':apellido', $this->apellido);
        $stmt->bindParam(':dui', $this->dui);
        $stmt->bindParam(':correo', $this->correo);
        $stmt->bindParam(':telefono', $this->telefono);
   
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    
    public function update(){
        //QUERY
        $query =' UPDATE '.$this->table. ' SET nombre = :nombre, apellido = :apellido, dui = :dui, correo = :correo, telefono = :telefono WHERE id_personal = :id_personal;';
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_personal = htmlspecialchars(strip_tags($this->id_personal));
        $this->nombre = htmlspecialchars(strip_tags($this->nombre));
        $this->apellido = htmlspecialchars(strip_tags($this->apellido));
        $this->dui = htmlspecialchars(strip_tags($this->dui));
        $this->correo = htmlspecialchars(strip_tags($this->correo));
        $this->telefono = htmlspecialchars(strip_tags($this->telefono));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_personal', $this->id_personal);
        $stmt->bindParam(':nombre', $this->nombre);
        $stmt->bindParam(':apellido', $this->apellido);
        $stmt->bindParam(':dui', $this->dui);
        $stmt->bindParam(':correo', $this->correo);
        $stmt->bindParam(':telefono', $this->telefono);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    
    public function delete(){
        //QUERY
        $query ='DELETE FROM '.$this->table. ' WHERE id_personal = :id_personal ;';
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_personal = htmlspecialchars(strip_tags($this->id_personal));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_personal', $this->id_personal);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    
    public function listaHistorial(){
        //Creamos la query
        $query = 'SELECT c.dia,c.hora,p.nombre,p.apellido FROM personal p IN';
        
         // Preparar Consulta
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->nombre);

        // Ejecutar la consulta
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // Tengo que probar PDO::FETCH_NUM
        // Set Propiedades
        $this->dia = $row['dia'];
        $this->hora = $row['hora'];
        $this->nombre = $row['nombre'];
        $this->apellido = $row['apellido'];
    }
    
    /*SELECT p.id_personal, p.nombre, p.apellido FROM personal p" +
                    " INNER JOIN usuario u ON u.id_personal = p.id_personal" +
                    " INNER JOIN rol rol ON rol.id_rol = u.id_rol " +
                    " WHERE rol.id_rol = 3*/
    
    public function listaDoctores(){
        $query = 'SELECT  p.id_personal, p.nombre, p.apellido FROM clinicautec.personal p' .
                    ' INNER JOIN clinicautec.usuario u ON u.id_personal = p.id_personal' .
                    ' INNER JOIN clinicautec.rol rol ON rol.id_rol = u.id_rol ' .
                    ' WHERE rol.id_rol = 3';
        //Preparar la consulta
        $stmt = $this->conn->prepare($query);
        //Ejecutar la consulta
        $stmt->execute();
        
        return $stmt;
    }
    
    //LISTA PRINCIPALES DE LAS VISTA DE DOCTOR
     public function listaDoctorCitas(){
       $query = 'SELECT c.id_cita,c.estado, exp.id_expediente, paci.nombre, paci.apellido, paci.correo, paci.telefono, c.estado ' . 'FROM clinicautec.cita c' .
                    ' INNER JOIN clinicautec.personal p ON p.id_personal = c.id_personal' .
                    ' INNER JOIN clinicautec.expediente exp ON exp.id_expediente = c.id_expediente' .
                    ' INNER JOIN clinicautec.paciente paci ON paci.id_paciente = exp.id_paciente where p.id_personal =? '
               . ' AND c.estado = "Pendiente" ORDER BY c.id_cita DESC;';
        
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Ejecutar la consulta
        $stmt->execute();

        return $stmt;
    } 
  
   
    
    /*    HASTA AQUI TERMINA LISTA DE DOCTORES         */
}