<?php

include_once(__DIR__.'/../models/Paciente.php');

class Expediente{
    private $conn;
    private $table = 'clinicautec.expediente';

    public $id_expediente;
    public $fecha_creacion;
    public $paciente;

    public function __construct($db)
    {
        $this->conn = $db;
        $this->paciente = new Paciente($db);
    }
    
    public function getPaciente(){
        return $this->paciente;
    }
    
    public function setPaciente($paciente){
        $this->paciente = $paciente;
    }
    
    public function read(){
        
        $query = 'SELECT e.id_expediente,e.fecha_creacion,p.id_paciente,p.nombre,p.apellido,p.dui,p.carnet FROM '
                . ' clinicautec.expediente e Inner Join clinicautec.paciente p on  e.id_paciente = p.id_paciente ORDER BY e.id_expediente  ';
        //Preparar la consulta
        $stmt = $this->conn->prepare($query);
        //Ejecutar la consulta
        $stmt->execute();
        
        return $stmt;
    }
    
     public function BuscarIDexpediente(){
        
        $query = 'SELECT e.id_expediente,e.fecha_creacion,p.id_paciente,p.nombre,p.apellido,p.dui,p.carnet FROM '
                . ' clinicautec.expediente e Inner Join clinicautec.paciente p on  e.id_paciente = p.id_paciente WHERE  e.id_expediente LIKE '
                . ' "%'.$this->id_expediente.'%"';
        //Preparar la consulta
        $stmt = $this->conn->prepare($query);
        //Ejecutar la consulta
        //VERIFICAMOS EL ID 
        $stmt->bindParam(1, $this->id_expediente);
        
        $stmt->execute();
        
         //Obtenemos en una variable $row toda la fila de la tabla
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        //SETIAMOS TODAS LA VARIABLES PARA LLENAR LA FILA
        $this->id_expediente = $row['id_expediente'];
        $this->fecha_creacion = $row['fecha_creacion'];
        $this->paciente->id_paciente = $row['id_paciente'];
        $this->paciente->nombre = $row['nombre'];
        $this->paciente->apellido = $row['apellido'];
        $this->paciente->dui = $row['dui'];
        $this->paciente->carnet = $row['carnet'];
        
    }
    
    public function create(){
        
        //QUERY
        $query ='INSERT INTO '. $this->table . ' VALUES(:id_expediente,:fecha_creacion,:id_paciente);';
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_expediente = htmlspecialchars(strip_tags($this->id_expediente));
        $this->fecha_creacion = htmlspecialchars(strip_tags($this->fecha_creacion));
        $this->paciente->id_paciente = htmlspecialchars(strip_tags($this->paciente->id_paciente));
        
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_expediente', $this->id_expediente);
        $stmt->bindParam(':fecha_creacion', $this->fecha_creacion);
        $stmt->bindParam(':id_paciente', $this->paciente->id_paciente);
       
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }

    public function update(){
        
        //QUERY
        $query ='UPDATE '. $this->table . ' SET id_expediente=:id_expediente,:id_paciente WHERE fecha_creacion =:fecha_creacion);';
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_expediente = htmlspecialchars(strip_tags($this->id_expediente));
        $this->fecha_creacion = htmlspecialchars(strip_tags($this->fecha_creacion));
        $this->paciente->id_paciente = htmlspecialchars(strip_tags($this->paciente->id_paciente));
        
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_expediente', $this->id_expediente);
        $stmt->bindParam(':fecha_creacion', $this->fecha_creacion);
        $stmt->bindParam(':id_paciente', $this->paciente->id_paciente);
       
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    
     public function delete(){
        //QUERY
        $query ='DELETE FROM '.$this->table. ' WHERE id_expediente = :id_expediente ;';
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_expediente = htmlspecialchars(strip_tags($this->id_expediente));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_expediente', $this->id_expediente);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }

    public function read_doctor1(){
        //Query
        $query = 'SELECT distinct c.id_expediente,p.nombre,p.telefono,p.apellido,p.correo,e.id_expediente,pe.id_personal '
                . ' FROM clinicautec.expediente as e  Inner  JOIN clinicautec.paciente as p on p.id_paciente = e.id_paciente '
                . ' Inner JOIN clinicautec.cita as c on  e.id_expediente = c.id_expediente Inner JOIN clinicautec.personal as pe '
                . ' on c.id_personal = pe.id_personal WHERE pe.id_personal = ? ORDER BY c.id_expediente ';
    
        // Preparar Consulta
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(1, $this->id);
        
        // Ejecutar la consulta
        $stmt->execute();

        return $stmt;
        
    }
    
    public function lista_consultaD(){
        
        $query = 'SELECT  '.$this->table;
    }
    
    public function listaPacientes(){
        $query = "SELECT ex.id_expediente, paci.nombre, paci.apellido, paci.dui, paci.carnet, paci.telefono, paci.fecha_nacimiento, paci.correo, paci.pass, paci.id_paciente FROM clinicautec.expediente ex INNER JOIN clinicautec.paciente paci ON paci.id_paciente = ex.id_paciente;";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
                
    }
}