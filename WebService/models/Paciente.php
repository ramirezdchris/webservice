<?php
class Paciente
{

    // Materia de la base de Datos
    private $conn;
    private $table = 'clinicautec.paciente';

    //Propiedades Paciente
    public $id_paciente;
    public $nombre;
    public $apellido;
    public $dui;
    public $carnet;
    public $telefono;
    public $fecha_nacimiento;
    public $correo;
    public $pass;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    // Get Pacientes
    public function read()
    {
        // Query
        $query = 'SELECT * FROM ' . $this->table;

        // Preparar Consulta
        $stmt = $this->conn->prepare($query);

        // Ejecutar la consulta
        $stmt->execute();

        return $stmt;
    }
    
    public function read_single()
    {
        // Query
        $query = 'SELECT * FROM ' . $this->table . ' WHERE id_paciente = ?';

        // Preparar Consulta
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Ejecutar la consulta
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // Tengo que probar PDO::FETCH_NUM

        // Set Propiedades
        $this->id_paciente = $row['id_paciente'];
        $this->nombre = $row['nombre'];
        $this->apellido = $row['apellido'];
        $this->dui = $row['dui'];
        $this->carnet = $row['carnet'];
        $this->telefono = $row['telefono'];
        $this->fecha_nacimiento = $row['fecha_nacimiento'];
        $this->correo = $row['correo'];
        $this->pass = $row['pass'];
    }

    public function create()
    {
        // Create Query
        $query = 'INSERT INTO ' . $this->table . ' VALUES(0,:nombre,:apellido,:dui,:carnet,:telefono,:fecha_nac,:correo,:pass);';
        $query2 = 'INSERT INTO clinicautec.expediente VALUES(:dui, CURDATE(),:id_paciente);';

        // Preparar sentencia
        $stmt = $this->conn->prepare($query);
        $s = $this->conn->prepare($query2);

        // Limpiar Datos
        $this->nombre = htmlspecialchars(strip_tags($this->nombre));
        $this->apellido = htmlspecialchars(strip_tags($this->apellido));
        $this->dui = htmlspecialchars(strip_tags($this->dui));
        $this->carnet = htmlspecialchars(strip_tags($this->carnet));
        $this->telefono = htmlspecialchars(strip_tags($this->telefono));
        $this->correo = htmlspecialchars(strip_tags($this->correo));
        $this->pass = htmlspecialchars(strip_tags($this->pass));

        //Mandando datos
        //$stmt->bindParam(':id', 0);
        $stmt->bindParam(':nombre', $this->nombre);
        $stmt->bindParam(':apellido', $this->apellido);
        $stmt->bindParam(':dui', $this->dui);
        $stmt->bindParam(':carnet', $this->carnet);
        $stmt->bindParam(':telefono', $this->telefono);
        $stmt->bindParam(':fecha_nac', $this->fecha_nacimiento);
        $stmt->bindParam(':correo', $this->correo);
        $stmt->bindParam(':pass', $this->pass);

        $ultimo = $this->ultimoPaciente() + 1;
        $s->bindParam(':dui', $this->dui);
        $s->bindParam(':id_paciente', $ultimo);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            $s->execute();
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }


    public function update()
    {
        // Create Query
        $query = 'UPDATE ' . $this->table . ' SET nombre = :nombre, apellido = :apellido, dui = :dui,carnet = :carnet, correo = :correo, telefono = :telefono, fecha_nacimiento = :fecha_nac, pass = :pass WHERE id_paciente = :id_paciente;';
        $query2 = 'UPDATE clinicautec.expediente SET id_expediente = :id_expediente WHERE id_paciente = :id_paciente';
        
        //UPDATE `clinicautec`.`expediente` SET `id_expediente`='0000000-0' WHERE `id_expediente`='00000000-0';

        // Preparar sentencia
        $stmt = $this->conn->prepare($query);
        $stmt2 = $this->conn->prepare($query2);

        // Limpiar Datos
        $this->nombre = htmlspecialchars(strip_tags($this->nombre));
        $this->apellido = htmlspecialchars(strip_tags($this->apellido));
        $this->dui = htmlspecialchars(strip_tags($this->dui));
        $this->carnet = htmlspecialchars(strip_tags($this->carnet));
        $this->correo = htmlspecialchars(strip_tags($this->correo));
        $this->telefono = htmlspecialchars(strip_tags($this->telefono));
        $this->fecha_nacimiento = htmlspecialchars(strip_tags($this->fecha_nacimiento));
        $this->correo = htmlspecialchars(strip_tags($this->correo));
        $this->correo = htmlspecialchars(strip_tags($this->correo));
        $this->id_paciente = htmlspecialchars(strip_tags($this->id_paciente));
        //Mandando datos
        //$stmt->bindParam(':id', 0);
        $stmt->bindParam(':nombre', $this->nombre);
        $stmt->bindParam(':apellido', $this->apellido);
        $stmt->bindParam(':dui', $this->dui);
        $stmt->bindParam(':carnet', $this->carnet);
        $stmt->bindParam(':telefono', $this->telefono);
        $stmt->bindParam(':fecha_nac', $this->fecha_nacimiento);
        $stmt->bindParam(':correo', $this->correo);
        $stmt->bindParam(':pass', $this->pass);
        $stmt->bindParam(':id_paciente' , $this->id_paciente);
        
        
        //Para actualizar la tabla expediente cuando se actualize el dui
        $stmt2->bindParam(':id_expediente' , $this->dui);
        $stmt2->bindParam(':id_paciente' , $this->id_paciente);

        // Ejecucion del la Query
        if ($stmt->execute() && $stmt2->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    

    public function delete()
    {
        // Create Query
        $query = 'DELETE FROM ' . $this->table . ' WHERE id_paciente = :id_paciente;';

        // Preparar sentencia
        $stmt = $this->conn->prepare($query);

        // Limpiar Datos
        $this->id_paciente = htmlspecialchars(strip_tags($this->id_paciente));
        
        //Mandando datos        
        $stmt->bindParam(':id_paciente' , $this->id_paciente);

        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }


    public function login(){
        $query = 'SELECT nombre, apellido FROM ' . $this->table . ' WHERE correo = ? AND pass = ?;';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->correo);
        $stmt->bindParam(2, $this->pass);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        //print_r($row);

        /*
        if(is_null($row['nombre'])){
            $crede = array("Mensaje" => 'Usuario no encontrado');
        }else{    
            $this->nombre = $row['nombre'];
            $this->apellido = $row['apellido'];        
            $crede = array('nombre' => $this->nombre, 'apellido' => $this->apellido);
        }
        */
        $this->nombre = $row['nombre'];
        $this->apellido = $row['apellido'];
        $crede = array('nombre' => $this->nombre, 'apellido' => $this->apellido);
        
        return $crede;
    }
    
    public function ultimoPaciente(){
        $query = 'SELECT * FROM clinicautec.paciente ORDER BY id_paciente DESC LIMIT 1;';
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_NUM);                
        //$this->nombre = $row['nombre'];
        //$this->apellido = $row['apellido'];
        //$crede = array('nombre' => $this->nombre, 'apellido' => $this->apellido);
        return $row[0];
    }
}