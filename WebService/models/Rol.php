<?php
class Rol{

    private $conn;
    private $table = 'clinicautec.rol';

    public $id_rol;
    public $rol;

    public function __construct($db)
    {
        $this->conn = $db;
    }
    
    //Metodo para seleccionar toda la tabla de rol
    // peticion de url Get rol
    public function read(){
        //Query
        $query = 'SELECT * FROM '.$this->table;
        //Preparar la consulta
        $stmt = $this->conn->prepare($query);
        //Ejecutar la consulta
        $stmt->execute();
        
        return $stmt;
    }
    
    public function  read_ID(){
        //QUERY
        $query = 'SELECT * FROM '.$this->table. ' WHERE id_rol = ?';
        
        //PREPARAR LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //VERIFICAMOS EL ID 
        $stmt->bindParam(1, $this->id);
        
        //Ejecutamos la consulta
        $stmt->execute();
        
        //Obtenemos en una variable $row toda la fila de la tabla
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        //SETIAMOS TODAS LA VARIABLES PARA LLENAR LA FILA
        $this->id_rol = $row['id_rol'];
        $this->rol = $row['rol'];
        
    }
    
    public function create(){
        //QUERY
        $query ='INSERT INTO '.$this->table. ' VALUES(0,:rol);';
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->rol = htmlspecialchars(strip_tags($this->rol));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':rol', $this->rol);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    
    public function update(){
        //create query para modificar datos del rol
        $query = 'UPDATE '. $this->table. ' SET rol = :rol WHERE id_rol = :id_rol';
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_rol = htmlspecialchars(strip_tags($this->id_rol));
        $this->rol = htmlspecialchars(strip_tags($this->rol));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_rol', $this->id_rol);
        $stmt->bindParam(':rol', $this->rol);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }

        public function delete(){
         // Create Query
        $query = 'DELETE FROM ' . $this->table . ' WHERE id_rol = :id_rol ;';

        // Preparar sentencia
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_rol = htmlspecialchars(strip_tags($this->id_rol));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_rol', $this->id_rol);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
}