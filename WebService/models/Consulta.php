<?php

include_once(__DIR__.'/../models/Cita.php');
include_once(__DIR__.'/../models/Personal.php');

class Consulta{
    private $conn;
    private $table = 'clinicautec.consulta';

    public $id_consulta;
    public $dia;
    public $hora;
    public $diagnostico;
    public $tratamiento;
    public $estado;
    public $cita;
    
    public $personal;
    
    public function __construct($db)
    {
        $this->conn = $db;
        $this->cita = new Cita($db);
        $this->personal = new Personal($db);
    }
    
    public function getCita(){
        return $this->cita;
    }
    
    public function setCita($cita){
        $this->cita = $cita;
    }
    function getPersonal() {
        return $this->personal;
    }

    function setPersonal($personal) {
        $this->personal = $personal;
    }

        
    
    // Get consulta
    public function read()
    {
        // Query
        /*$query = 'SELECT c.id_consulta,c.dia,c.hora,c.diagnostico,c.tratamiento,c.estado,ci.id_cita,c.id_cita FROM '.$this->table. '  as c inner join '
                . 'clinicautec.cita as ci ON c.id_cita = ci.id_cita' ;*/
        $query = 'SELECT * FROM clinicautec.consulta ';
      
        // Preparar Consulta
        $stmt = $this->conn->prepare($query);

        // Ejecutar la consulta
        $stmt->execute();

        return $stmt;
    }
	
	public function read_single()
    {
        // Query
        $query = 'SELECT * FROM ' . $this->table . ' WHERE id_consulta = ?';

        // Preparar Consulta
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Ejecutar la consulta
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // Tengo que probar PDO::FETCH_NUM
        // Set Propiedades
        $this->id_consulta = $row['id_consulta'];
        $this->dia = $row['dia'];
        $this->hora = $row['hora'];
        $this->diagnostico = $row['diagnostico'];
        $this->tratamiento = $row['tratamiento'];
        $this->estado = $row['estado'];
        $this->cita->id_cita = $row['id_cita'];
    }
	
    public function create(){
        // Create Query
        $query = 'INSERT INTO ' . $this->table . ' VALUES(0,:dia,:hora,:diagnostico,:tratamiento,:estado,:id_consulta);';

        // Preparar sentencia
        $stmt = $this->conn->prepare($query);

        // Limpiar Datos
        $this->dia = htmlspecialchars(strip_tags($this->dia));
        $this->hora = htmlspecialchars(strip_tags($this->hora));
        $this->diagnostico = htmlspecialchars(strip_tags($this->diagnostico));
        $this->tratamiento = htmlspecialchars(strip_tags($this->tratamiento));
        $this->estado = htmlspecialchars(strip_tags($this->estado));
        $this->cita->id_cita = htmlspecialchars(strip_tags($this->cita->id_cita));
        

        //Mandando datos
        $stmt->bindParam(':dia', $this->dia);
        $stmt->bindParam(':hora', $this->hora);
        $stmt->bindParam(':diagnostico', $this->diagnostico);
        $stmt->bindParam(':tratamiento', $this->tratamiento);
	$stmt->bindParam(':estado', $this->estado);
        $stmt->bindParam(':id_consulta', $this->cita->id_cita);

        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
	
    public function update()
    {
        // Create Query
        $query = 'UPDATE ' . $this->table . ' SET dia = :dia, hora = :hora, diagnostico = :diagnostico, tratamiento = :tratamiento, estado = :estado, id_personal = :id_personal, id_expediente = :id_expediente WHERE id_consulta = :id_consulta;';

        // Preparar sentencia
        $stmt = $this->conn->prepare($query);

         // Limpiar Datos
        $this->dia = htmlspecialchars(strip_tags($this->dia));
        $this->hora = htmlspecialchars(strip_tags($this->hora));
        $this->diagnostico = htmlspecialchars(strip_tags($this->diagnostico));
        $this->tratamiento = htmlspecialchars(strip_tags($this->tratamiento));
		$this->estado = htmlspecialchars(strip_tags($this->estado));
        $this->id_personal = htmlspecialchars(strip_tags($this->id_personal));
        $this->id_expediente = htmlspecialchars(strip_tags($this->id_expediente));

         //Mandando datos
        $stmt->bindParam(':dia', $this->dia);
        $stmt->bindParam(':hora', $this->hora);
        $stmt->bindParam(':diagnostico', $this->diagnostico);
        $stmt->bindParam(':tratamiento', $this->tratamiento);
		$stmt->bindParam(':estado', $this->estado);
        $stmt->bindParam(':id_personal', $this->id_personal);
        $stmt->bindParam(':id_expediente', $this->id_expediente);

        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
	
	public function delete()
    {
        // Create Query
        $query = 'DELETE FROM ' . $this->table . ' WHERE id_consulta = :id_consulta;';

        // Preparar sentencia
        $stmt = $this->conn->prepare($query);

        // Limpiar Datos
        $this->id_paciente = htmlspecialchars(strip_tags($this->id_paciente));
        
        //Mandando datos        
        $stmt->bindParam(':id_consulta' , $this->id_consulta);

        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    
    public function HistorialConsultaD(){
        
        $query = 'Select c.id_consulta,c.dia,c.hora,c.id_cita,ci.id_cita,ci.id_expediente,ci.id_personal,p.id_personal,p.nombre,p.apellido from '.$this->table.' as c inner join clinicautec.cita  as ci '
                . ' on c.id_cita= ci.id_cita inner join clinicautec.expediente as e on ci.id_expediente = e.id_expediente '
                . 'inner join clinicautec.personal as p on ci.id_personal = p.id_personal WHERE ci.id_expediente = ? ORDER BY c.id_consulta DESC';
        
        // Preparar Consulta
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Ejecutar la consulta
        $stmt->execute();
           //Cuando solo se selecione un historial sera esto comentado
//        $row = $stmt->fetch(PDO::FETCH_ASSOC);
//
//        // Tengo que probar PDO::FETCH_NUM
//        // Set Propiedades
//        $this->id_consulta = $row['id_consulta'];
//        $this->dia = $row['dia'];
//        $this->hora = $row['hora'];
//        
//        //Obtenemos los datos relacionados
//        $this->cita->expediente = $row['id_expediente'];
//        //$this->cita->personal = $row['id_personal'];
//        
//        //Datos del doctor
//        $this->cita->personal->nombre = $row['nombre'];
//        //$this->cita->id_cita->id_cita = $row['id_cita'];
//        //$this->cita->persona->id_personal = $row['p.id_personal'];
//        
        return $stmt;
    }
    
     public function selectHistorialD(){
       
        
        // Query
        $query = 'Select c.dia,c.hora,c.diagnostico,c.tratamiento,c.estado,c.id_consulta,e.id_expediente'
                . ' from '.$this->table.' as c inner join clinicautec.cita  as ci '
                . ' on c.id_cita= ci.id_cita inner join clinicautec.expediente as e on ci.id_expediente= e.id_expediente  WHERE e.id_expediente = ? ';

        // Preparar Consulta
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Ejecutar la consulta
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // Tengo que probar PDO::FETCH_NUM
        // Set Propiedades
        $this->id_consulta = $row['id_consulta'];
        $this->dia = $row['dia'];
        $this->hora = $row['hora'];
        $this->diagnostico = $row['diagnostico'];
        $this->tratamiento = $row['tratamiento'];
        $this->estado = $row['estado'];
        $this->cita->id_cita = $row['id_cita'];
        $this->cita->expediente = $row['id_expediente'];
    }
    
    /*  Lista de Mis Consultas del Modulo Paciente  */
    public function HistorialPaciente(){
        
        $query = 'SELECT c.id_consulta,c.dia,c.hora,c.estado,p.nombre,p.apellido,e.id_expediente FROM clinicautec.consulta c '
                . 'Inner join clinicautec.cita ci on c.id_cita = ci.id_cita Inner join clinicautec.personal p '
                . 'on ci.id_personal = p.id_personal Inner join clinicautec.expediente e on ci.id_expediente = e.id_expediente'
                . ' WHERE e.id_expediente=?  ORDER BY c.id_consulta DESC';
      
        $stmt = $this->conn->prepare($query);

         //VERIFICAMOS EL ID 
        $stmt->bindParam(1, $this->id);
        
        $stmt->execute();
       
        
        return  $stmt;
    }
    
    
    /* Fin de la utilizacion de las Query */
}