<?php

include_once(__DIR__.'/../models/Expediente.php');

class Solicitudes{
    private $conn;
    private $table = 'clinicautec.solicitudes';

    public $id_solicitud;
    public $dia;
    public $hora;
    public $sintoma;
    public $estado;
    public $mensaje;
    public $expediente;

    public function __construct($db)
    {
        $this->conn = $db;
        $this->expediente = new Expediente($db);
    }
    
    public function getExpediente(){
        return $this->expediente;
    }
    
    public function setExpediente($expediente){
        $this->expediente = $expediente;
    }
    
    public function listaSoloicitudPaciente(){
        
        $query ="SELECT s.id_solicitud,s.dia,s.hora,s.sintoma,s.estado,s.mensaje,s.id_expediente,e.id_expediente FROM ".$this->table.' as s '
                . ' INNER JOIN clinicautec.expediente as e on s.id_expediente= e.id_expediente WHERE e.id_expediente = ? ORDER BY s.id_solicitud DESC';
        
        $stmt = $this->conn->prepare($query);

         //VERIFICAMOS EL ID 
        $stmt->bindParam(1, $this->id);
        
        $stmt->execute();
       
        
        return  $stmt;
    }
    
    public function InsertarSolicitudPaciente(){
        
        $query = ' INSERT INTO ' .$this->table. ' VALUES(0,:dia, :hora, :sintoma, :estado, :mensaje, :id_expediente);';
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->dia = htmlspecialchars(strip_tags($this->dia));
        $this->hora = htmlspecialchars(strip_tags($this->hora));
        $this->sintoma = htmlspecialchars(strip_tags($this->sintoma));
        $this->estado = htmlspecialchars(strip_tags($this->estado));
		$this->mensaje = htmlspecialchars(strip_tags($this->mensaje));
        $this->expediente->id_expediente = htmlspecialchars(strip_tags($this->expediente->id_expediente));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':dia', $this->dia);
        $stmt->bindParam(':hora', $this->hora);
        $stmt->bindParam(':sintoma', $this->sintoma);
        $stmt->bindParam(':estado', $this->estado);
        $stmt->bindParam(':mensaje' , $this->mensaje);
        $stmt->bindParam(':id_expediente', $this->expediente->id_expediente);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    
    public function ActualizarSolicitudPaciente(){
        
        $query = "UPDATE ".$this->table. " SET dia=:dia, hora=:hora, sintoma=:sintoma, estado=:estado, id_expediente=:id_expediente WHERE id_solicitud = :id_solicitud ;";
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_solicitud = htmlspecialchars(strip_tags($this->id_solicitud));
        $this->dia = htmlspecialchars(strip_tags($this->dia));
        $this->hora = htmlspecialchars(strip_tags($this->hora));
        $this->sintoma = htmlspecialchars(strip_tags($this->sintoma));
        $this->estado = htmlspecialchars(strip_tags($this->estado));
        $this->expediente->id_expediente = htmlspecialchars(strip_tags($this->expediente->id_expediente));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_solicitud', $this->id_solicitud);
        $stmt->bindParam(':dia', $this->dia);
        $stmt->bindParam(':hora', $this->hora);
        $stmt->bindParam(':sintoma', $this->sintoma);
        $stmt->bindParam(':estado', $this->estado);
        $stmt->bindParam(':id_expediente', $this->expediente->id_expediente);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    public function delete(){
        //QUERY
        $query ='DELETE FROM '.$this->table. ' WHERE id_solicitud = :id_solicitud ;';
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_solicitud = htmlspecialchars(strip_tags($this->id_solicitud));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_solicitud', $this->id_solicitud);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    
    
    public function actualizarSolicitudPorEnfermera(){
        
        $query = "UPDATE ".$this->table. " SET estado=:estado, mensaje =:mensaje WHERE id_solicitud = :id_solicitud;";        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_solicitud = htmlspecialchars(strip_tags($this->id_solicitud));
        //$this->dia = htmlspecialchars(strip_tags($this->dia));
        //$this->hora = htmlspecialchars(strip_tags($this->hora));
        //$this->sintoma = htmlspecialchars(strip_tags($this->sintoma));       
        $this->estado = htmlspecialchars(strip_tags($this->estado));
        $this->mensaje = htmlspecialchars(strip_tags($this->mensaje));
        $this->expediente->id_expediente = htmlspecialchars(strip_tags($this->expediente->id_expediente));
        
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_solicitud', $this->id_solicitud);
        //$stmt->bindParam(':dia', $this->dia);
        //$stmt->bindParam(':hora', $this->hora);
       // $stmt->bindParam(':sintoma', $this->sintoma);
        $stmt->bindParam(':estado', $this->estado);
        $stmt->bindParam(':mensaje' , $this->mensaje);
        //$stmt->bindParam(':id_expediente', $this->expediente->id_expediente);
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    
    public function aprobarSolicitud(){
        
        $query = "UPDATE ".$this->table. " SET estado =:estado WHERE id_solicitud = :id_solicitud;";        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->id_solicitud = htmlspecialchars(strip_tags($this->id_solicitud));           
        $this->estado = htmlspecialchars(strip_tags($this->estado));
                     
        //MANDAMOS LOS DATOS
        $stmt->bindParam(':id_solicitud', $this->id_solicitud);
        $stmt->bindParam(':estado', $this->estado);
                                
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }
    
    public function listaSolicitudEnfermera(){
        
        $query ="SELECT s.id_solicitud,s.dia,s.hora,s.sintoma,s.id_expediente,e.id_expediente FROM ".$this->table.' as s '
                . ' INNER JOIN clinicautec.expediente as e on s.id_expediente= e.id_expediente WHERE s.estado = "Pendiente"';
        
        $stmt = $this->conn->prepare($query);
         //VERIFICAMOS EL ID 
        //$stmt->bindParam(1, $this->id);       
        $stmt->execute();
       
        
        return  $stmt;
    }
}