<?php


include_once(__DIR__.'/../models/Personal.php');
include_once(__DIR__.'/../models/Rol.php');
include_once(__DIR__.'/../models/Paciente.php');
include_once(__DIR__.'/../models/Expediente.php');

class Usuario{

    private $conn;
    private $table = 'clinicautec.usuario';

    public $usuario;
    public $pass;
    public $personal;
    public $rol;
    public $paciente;
    public $expediente;


    public function __construct($db)
    {
        $this->conn = $db;
        $this->personal = new Personal($db);
        $this->rol = new Rol($db);
        $this->paciente = new Paciente($db);
        $this->expediente = new Expediente($db);
    }
    
    public function getPersonal(){
        return $this->personal;
    }
    
    public function setPersonal($personal){
        $this->personal = $personal;
    }
    
    public function getRol(){
        return $this->rol;
    }
    
    public function setRol($rol){
        $this->rol = $rol;
    }

    public function login($user = null, $pass = null){
        
        $query = 'SELECT p.id_personal, p.nombre, p.apellido, id_rol FROM clinicautec.usuario'  . ' user INNER JOIN ' . 'clinicautec.personal p ON p.id_personal = user.id_personal' . ' WHERE user.usuario = ?' .' AND user.pass = ?';
        
        $query2 = 'SELECT e.id_expediente, p.nombre, p.apellido FROM clinicautec.paciente p INNER JOIN clinicautec.expediente e ON e.id_paciente = p.id_paciente  WHERE' .' p.correo = ? AND p.pass = ?';
        
        // Primera consulta
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $user, PDO::PARAM_STR);
        $stmt->bindParam(2, $pass, PDO::PARAM_STR);
        $stmt->execute();
        
        
        // Segunda consulta
        $stmt2 = $this->conn->prepare($query2);
        $stmt2->bindParam(1, $user, PDO::PARAM_STR);
        $stmt2->bindParam(2, $pass, PDO::PARAM_STR);
        $stmt2->execute();
        
        $row = $stmt->fetch(PDO::FETCH_ASSOC);  
        $this->personal->id_personal = $row['id_personal'];
        $this->personal->nombre = $row['nombre'];
        $this->personal->apellido = $row['apellido'];
        $this->rol->id_rol = $row['id_rol'];
        //$this->setRol($rol);
        //$this->setPersonal($personal);
        
        
        $row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
        $this->expediente->id_expediente = $row2['id_expediente'];
        $this->paciente->nombre = $row2['nombre'];
        $this->paciente->apellido = $row2['apellido'];        
        
        //echo $row2['nombre'];
               
        //print_r($row);
        //print_r($row2);
        
        if(($row == null) && ($row2 == null)){           
            $crede = array('id_usuario' => 0,'nombre' => 0, 'apellido' =>0 , 'rol' => 0);
        }else if($row2 != null){
            $crede = array('id_usuario' => $this->expediente->id_expediente, 'nombre' => $this->paciente->nombre, 'apellido' => $this->paciente->apellido, 'rol' => '4');
            
        }else if($row != null){
            $crede = array('id_usuario' => $this->personal->id_personal, 'nombre' => $this->personal->nombre, 'apellido' => $this->personal->apellido, 'rol' => $this->rol->id_rol);          
        }
        return $crede;
    }


    public function login2(){
        $query = 'SELECT id_rol FROM ' . $this->table . ' WHERE usuario = ? AND pass = ?;';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->usuario);
        $stmt->bindParam(2, $this->pass);
        
        if($stmt->execute()){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->id_rol = $row['id_rol'];
            
        }else{
            $this->id_rol = -1;
        }
        
    }
    
    public function InsertarUsuario(){
        
        $query = "INSERT INTO ".$this->table. " VALUES(:usuario,:pass,:id_personal,:id_rol);";
        
        
        //PREPARAMOS LA CONSULTA
        $stmt = $this->conn->prepare($query);
        
        //LIMPIAMOS LOS DATOS
        $this->usuario = htmlspecialchars(strip_tags($this->usuario));
        $this->pass = htmlspecialchars(strip_tags($this->pass));
        $this->personal = htmlspecialchars(strip_tags($this->personal));
        $this->rol = htmlspecialchars(strip_tags($this->rol));
        
        
        //MANDAMOS LOS DATOS
        $ultimo = $this->ultimoPersonal();
        $stmt->bindParam(':usuario', $this->usuario);
        $stmt->bindParam(':pass', $this->pass);
        $stmt->bindParam(':id_rol', $this->rol);
        $stmt->bindParam(':id_personal', $ultimo);
       
        
        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }

        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
        
    }
    
    public function ultimoPersonal(){
        $query = 'SELECT id_personal FROM clinicautec.personal ORDER BY id_personal DESC LIMIT 1;';
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_NUM);                
        //$this->nombre = $row['nombre'];
        //$this->apellido = $row['apellido'];
        //$crede = array('nombre' => $this->nombre, 'apellido' => $this->apellido);
        return $row[0];
    }
    
}

?>