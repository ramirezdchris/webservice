<?php

include_once(__DIR__.'/../models/Personal.php');
include_once(__DIR__.'/../models/Expediente.php');
include_once(__DIR__.'/../models/Consulta.php');

class Cita{
    private $conn;
    private $table = 'clinicautec.cita';

    public $id_cita;
    public $dia;
    public $hora;
    public $estado;
    public $personal;
    public $expediente;
    
    public function __construct($db)
    {
        $this->conn = $db;
        $this->personal = new Personal($db);
        $this->expediente = new Expediente($db);
    }
    
    public function getPersonal(){
        return $this->personal;
    }
    
    public function setPersonal($personal){
        $this->personal = $personal;
    }
    
    public function getExpediente(){
        return $this->expediente;
    }
    
    public function setExpediente($expediente){
        $this->expediente = $expediente;
    }
    
    

    public function create(){
        $query = 'INSERT INTO ' .$this->table . ' VALUES(0,:dia,:hora,:estado,:idp,:idex);';
        $stmt = $this->conn->prepare($query);
        $this->dia = htmlspecialchars(strip_tags($this->dia));
        $this->hora = htmlspecialchars(strip_tags($this->hora));
        $this->estado = htmlspecialchars(strip_tags($this->estado));
        $this->personal->id_personal = htmlspecialchars(strip_tags($this->personal->id_personal));
        $this->expediente->id_expediente = htmlspecialchars(strip_tags($this->expediente->id_expediente));
        
        $stmt->bindParam(':dia', $this->dia);
        $stmt->bindParam(':hora', $this->hora);
        $stmt->bindParam(':estado', $this->estado);
        $stmt->bindParam(':idp', $this->personal->id_personal);
        $stmt->bindParam(':idex', $this->expediente->id_expediente);
        
        
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
        //print_r('Error: ' , $stmt->error);
        
    }
    
    public function update(){
        $query = 'UPDATE ' .$this->table . ' SET dia = :dia, hora = :hora, id_personal = :idp WHERE id_cita = :id_cita;';
        $stmt = $this->conn->prepare($query);
        $this->dia = htmlspecialchars(strip_tags($this->dia));
        $this->hora = htmlspecialchars(strip_tags($this->hora));
        $this->personal->id_personal = htmlspecialchars(strip_tags($this->personal->id_personal));
        $this->expediente->id_expediente = htmlspecialchars(strip_tags($this->expediente->id_expediente));
        
        $stmt->bindParam(':dia', $this->dia);
        $stmt->bindParam(':hora', $this->hora);
        //$stmt->bindParam(':estado', $this->estado);
        $stmt->bindParam(':idp', $this->personal->id_personal);
        $stmt->bindParam(':id_cita', $this->id_cita);
        
        
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
        //print_r('Error: ' , $stmt->error);
        
    }    
    
    public function consultarCita(){
        $q = 'SELECT * FROM clinicautec.cita WHERE hora = ? AND dia = ? AND id_personal = ? AND id_expediente = ?';
        $s = $this->conn->prepare($q);              
        $s->bindParam(1, $this->hora, PDO::PARAM_STR);
        $s->bindParam(2, $this->dia, PDO::PARAM_STR);
        $s->bindParam(3, $this->personal->id_personal, PDO::PARAM_STR);
        $s->bindParam(4, $this->expediente->id_expediente, PDO::PARAM_STR);
        $s->execute();
        $row2 = $s->fetch(PDO::FETCH_ASSOC);
        
        if($row2 == null){
            return false; 
        }else{
            return true;
        }       
    }
    
    public function delete(){
        $q = 'DELETE FROM clinicautec.cita WHERE id_cita = ?';
        $s = $this->conn->prepare($q);              
        $s->bindParam(1, $this->id_cita, PDO::PARAM_STR);              
        
        if($s->execute()){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function listaCitas(){
        $query = 'SELECT Distinct  c.id_cita, c.dia, c.hora, p.nombre, exp.id_expediente, paci.nombre, paci.apellido ' . 'FROM clinicautec.cita c' .
                    ' INNER JOIN clinicautec.personal p ON p.id_personal = c.id_personal' .
                    ' INNER JOIN clinicautec.expediente exp ON exp.id_expediente = c.id_expediente' .
                    ' INNER JOIN clinicautec.paciente paci ON paci.id_paciente = exp.id_paciente WHERE c.estado = "Pendiente";';
        
        $stmt = $this->conn->prepare($query);        
        $stmt->execute();

        return $stmt;
    }
    
    /* LISTA DEL MODULO DOCTOR */
    public function listaCitasHistorial(){
        $query = 'SELECT c.id_cita,c.estado,exp.id_expediente, paci.nombre, paci.apellido, paci.correo, paci.telefono ' . 'FROM clinicautec.cita c' .
                    ' INNER JOIN clinicautec.personal p ON p.id_personal = c.id_personal' .
                    ' INNER JOIN clinicautec.expediente exp ON exp.id_expediente = c.id_expediente' .
                    ' INNER JOIN clinicautec.paciente paci ON paci.id_paciente = exp.id_paciente where c.estado= "Aprobado" ;';
        
        $stmt = $this->conn->prepare($query);        
        $stmt->execute();

        return $stmt;
    }
    
    public function listaPacientesAsignados(){
        $query = 'SELECT DISTINCT  c.id_cita,c.estado, c.id_expediente, paci.nombre, paci.apellido, paci.correo, paci.telefono ' . 'FROM clinicautec.cita AS c' .
                    ' INNER JOIN clinicautec.personal p ON p.id_personal = c.id_personal' .
                    ' INNER JOIN clinicautec.expediente exp ON exp.id_expediente = c.id_expediente' .
                    ' INNER JOIN clinicautec.paciente paci ON paci.id_paciente = exp.id_paciente where c.estado= "Aprobado" ;';
        
        $stmt = $this->conn->prepare($query);        
        $stmt->execute();

        return $stmt;
    }
    
    public function CambiarEstado(){
        
        $query = "UPDATE ".$this->table. " SET estado = :estado WHERE id_cita = :id_cita ;";
        
        // Preparar sentencia
        $stmt = $this->conn->prepare($query);
        
        
         // Limpiar Datos
        $this->id_cita = htmlspecialchars(strip_tags($this->id_cita));
        $this->estado = htmlspecialchars(strip_tags($this->estado));
        
         //Mandando datos
        $stmt->bindParam(':id_cita', $this->id_cita);
        $stmt->bindParam(':estado', $this->estado);
      

        // Ejecucion del la Query
        if ($stmt->execute()) {
            return true;
        }
        // Imprimir error si se genero
        printf('Error :', $stmt->error);

        return false;
    }


    /* FIN DE LISTAS OCUPADAS */
    
   
}