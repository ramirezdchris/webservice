<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Usuario.php';


// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$usuario = new Usuario($db);


// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

$usuario->usuario = $data->usuario;
$usuario->pass = $data->pass;
$usuario->rol = $data->rol;

// Crear paciente
if ($usuario->InsertarUsuario()) {
    echo json_encode(array('message' => 'Personal Creado con Exito'));
} else {
    echo json_encode(array('message' => 'Personal no pudo ser creado'));
}
