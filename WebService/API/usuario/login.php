<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Usuario.php';
include_once '../../models/Personal.php';
include_once '../../models/Rol.php';
include_once '../../models/Expediente.php';


// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$usuario = new Usuario($db);
$personal = new Personal($db);
$rol = new Rol($db);
$expediente = new Expediente($db);

$user = isset($_GET['user']) ? $_GET['user'] : die();
$pass = isset($_GET['pass']) ? $_GET['pass'] : die();

//$usuario->login($user,$pass);

// Crear Array
/*
$usuario_arr = array(
    'rol' => $usuario->id_rol,
    'nombre' => $usuario->nombre,
    'apellido' => $usuario->apellido
);*/

$usuario_arr = $usuario->login($user, $pass);

// Creacion de JSON
print_r(json_encode($usuario_arr));