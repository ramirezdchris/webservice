<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Consulta.php';
include_once '../../models/Cita.php';
include_once '../../models/Personal.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$consulta = new Consulta($db);

//GET BUSCAR POR CARNET
$consulta->id = isset($_GET['id']) ? $_GET['id'] : die();

//hacemos el llamado de la funcion

$result = $consulta->HistorialPaciente();
$num = $result->rowCount();

if($num > 0){
    $solicitudes_arr = array();
    $solicitudes_arr['data'] = array();
    
    while($row = $result->fetch(PDO::FETCH_NUM)){
        extract($row);
        $solicitudes_item = array(
            'id_consulta' => $row[0],  
            'dia' => $row[1],
            'hora' => $row[2],
            'estado' => $row[3],
            'nombre' => $row[4],
            'apellido' => $row[5],
            'Expediente' => $row[6]
           
            
        );
        array_push($solicitudes_arr['data'], $solicitudes_item);
    }
    echo json_encode($solicitudes_arr);
}else{
    echo json_encode(
            array('mensaje' => 'Cita no registradas')
    );
}