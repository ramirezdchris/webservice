<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/database.php';
include_once '../../models/Consulta.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$consulta = new Consulta($db);
$cita = new Cita($db);

$data = json_decode(file_get_contents("php://input"));

$consulta->dia = $data->dia;
$consulta->hora = $data->hora;
$consulta->diagnostico = $data->diagnostico;
$consulta->tratamiento = $data->tratamiento;
$consulta->estado = $data->estado;
$cita->id_cita = $data->id_cita;
$consulta->setCita($cita);

if($consulta->create()){
    echo json_encode(array('message' => 'Consulta insertada'));
}   
else {
    echo json_encode(array('message' => 'Error al insertar la sita'));
}

