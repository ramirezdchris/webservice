<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Consulta.php';
include_once '../../models/Cita.php';
include_once '../../models/Expediente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$consulta = new Consulta($db);
$cita = new Cita($db);
//$expediente = new Expediente($db);

//GET BUSCAR POR ID
$consulta->id = isset($_GET['id']) ? $_GET['id'] : die();

//hacemos el llamado de la funcion
$consulta->selectHistorialD();

$num = $result->rowCount();

// Verificar si hay datos
if ($num > 0 ) {
    $consulta_arr = array();
    $consulta_arr['data'] = array();
    
    while ($row = $result->fetch(PDO::FETCH_ASSOC) ) {
        extract($row);
        $consulta_item = array(
            'id_consulta' => $row[0],
            'dia' => $row[1],
            'hora' => $row[2],
            'diagnostico' => $row[3],
            'tratamiento' => $row[4],
            'estado' => $row[5],
            'Expediente' => $row[6]
           
         
        );
         // Meter la Data
        array_push($consulta_arr['data'], $consulta_item);
    }
     
     // Devolver JSON
    echo json_encode($consulta_arr);
}else{
    echo json_encode(
        array('message' => 'Datos de consulta no encontrados')
    );
}

//$consulta_arra = array(
//    'id_consulta' => $consulta->id_consulta,
//    'dia' => $consulta->dia,
//    'hora' => $consulta->hora,
//    'diagnostico' => $consulta->diagnostico,
//    'tratamiento' => $consulta->tratamiento,
//    'estado' => $consulta->estado,
//    'Expediente' => $consulta->cita->expediente
//);
//// Creacion de JSON
//print_r(json_encode($consulta_arra));