<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Consulta.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$consulta = new Consulta($db);
$cita = new Cita($db);

//GET BUSCAR POR ID
$consulta->id = isset($_GET['id']) ? $_GET['id'] : die();

//hacemos el llamado de la funcion
$consulta->read_single();

$consulta_arr['data'] = array();
$consulta_arra = array(
    'id_consulta' => $consulta->id_consulta,
    'dia' => $consulta->dia,
    'hora' => $consulta->hora,
    'diagnostico' => $consulta->diagnostico,
    'tratamiento' => $consulta->tratamiento,
    'estado' => $consulta->estado,
    'id_cita' => $consulta->getCita()->id_cita
);
array_push($consulta_arr['data'], $consulta_arra);
// Creacion de JSON
print_r(json_encode($consulta_arr));