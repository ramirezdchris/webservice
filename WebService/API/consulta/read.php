<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Consulta.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$consulta = new Consulta($db);
$cita = new Cita($db);

//Query
$result = $consulta->read();
//Get numero de columnas
$num = $result->rowCount();

// Verificar si hay datos
if ($num > 0 ) {
    $consulta_arr = array();
    $consulta_arr['data'] = array();
    
    while ($row = $result->fetch(PDO::FETCH_ASSOC) ) {
        extract($row);
        $consulta_item = array(
            'id_consulta' => $id_consulta,
            'dia' => $dia,
            'hora' => $hora,
            'diagnostico' => $diagnostico,
            'tratamiento' => $tratamiento,
            'estado' => $estado,
            'id_cita' => $id_cita
        );
         // Meter la Data
        array_push($consulta_arr['data'], $consulta_item);
    }
     
     // Devolver JSON
    echo json_encode($consulta_arr);
}else{
    echo json_encode(
        array('message' => 'Datos de consulta no encontrados')
    );
}

