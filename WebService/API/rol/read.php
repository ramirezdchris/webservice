<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Rol.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$rolclass = new Rol($db);

//QUERY
$result = $rolclass->read();

//Get numero de columnas
$num = $result->rowCount();

if ($num > 0) {
    $rol_arra = array();
    $rol_arra['data'] = array();
    
     while ($row = $result->fetch(PDO::FETCH_ASSOC)){
         extract($row);
         $rol_item = array(
             'id_rol' => $id_rol,
             'rol' => $rol
         );
         // Meter la Data
        array_push($rol_arra['data'], $rol_item);
     }
     // Devolver JSON
    echo json_encode($rol_arra);
}else{
    echo json_encode(
        array('message' => 'Rol no encontrados')
    );
}
