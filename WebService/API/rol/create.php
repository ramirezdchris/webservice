<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Rol.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

//Pasamos los parametro de la conexio 
$rolclass = new Rol($db);

//Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));
$rolclass->rol = $data->rol;

// Crear Rol
if ($rolclass->create()) {
    echo json_encode(array('message' => 'Rol Creado Exitosamente'));
} else {
    echo json_encode(array('message' => 'El rol no pudo ser creado'));
}


