<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/database.php';
include_once '../../models/Rol.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$rolclass = new Rol($db);

//GET BUSCAR POR ID
$rolclass->id = isset($_GET['id']) ? $_GET['id'] : die();

//hacemos el llamado de la funcion
$rolclass->read_ID();

//Creamos el Array
$rol_arra = array(
    'id_rol' => $rolclass->id_rol,
    'rol' => $rolclass->rol
);
// Creacion de JSON
print_r(json_encode($rol_arra));
