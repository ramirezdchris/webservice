<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Solicitudes.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$solicitudes = new Solicitudes($db);

// Get ID
$solicitudes->id = isset($_GET['id']) ? $_GET['id'] : die();

//LLamamos al metodo
$result = $solicitudes->listaSoloicitudPaciente();

$num = $result->rowCount();

if($num > 0){
    $solicitudes_arr = array();
    $solicitudes_arr['data'] = array();
    
    while($row = $result->fetch(PDO::FETCH_NUM)){
        extract($row);
        $solicitudes_item = array(
            'id_solicitud' => $row[0],  
            'dia' => $row[1],
            'hora' => $row[2],
            'sintoma' => $row[3],
            'estado' => $row[4],
            'mensaje' => $row[5],
            'id_expediente' => $row[6]
           
            
        );
        array_push($solicitudes_arr['data'], $solicitudes_item);
    }
    echo json_encode($solicitudes_arr);
}else{
    echo json_encode(
            array('mensaje' => 'Cita no registradas')
    );
}
