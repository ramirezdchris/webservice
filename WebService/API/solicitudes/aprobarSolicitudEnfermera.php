<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Solicitudes.php';
include_once '../../models/Expediente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$solicitudes = new Solicitudes($db);
$expediente = new Expediente($db);

// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

//Obtener el ID para modificar el archivo
$solicitudes->id_solicitud = $data->id_solicitud;
$solicitudes->estado = $data->estado;


// Crear paciente
if ($solicitudes->aprobarSolicitud()) {
    echo json_encode(array('message' => 'Solicitud Actualizada con Exito'));
} else {
    echo json_encode(array('message' => 'No pudo ser actualizar la Solicitud'));
}