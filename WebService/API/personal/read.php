<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Personal.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$personal = new Personal($db);

//Query
$result = $personal->read();

//Get numero de columnas
$num = $result->rowCount();

// Verificar si hay datos
if ($num > 0) {
    // Array
    $personal_arr = array();
    $personal_arr['data'] = array();
    
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        $personal_item = array(
            'id_personal' => $id_personal,
            'nombre' => $nombre,
            'apellido' => $apellido,
            'dui' => $dui,
            'correo' => $correo,
            'telefono' => $telefono,
        );
         // Meter la Data
        array_push($personal_arr['data'], $personal_item);
    }
    // Devolver JSON
    echo json_encode($personal_arr);
}else {
    echo json_encode(
        array('message' => 'Personal no encontrados')
    );
}

