<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Personal.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$personal = new Personal($db);


// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

//Obtener el ID para modificar el archivo
$personal->id_personal = $data->id_personal;

$personal->nombre = $data->nombre;
$personal->apellido = $data->apellido;
$personal->dui = $data->dui;
$personal->correo = $data->correo;
$personal->telefono = $data->telefono;

// Crear paciente
if ($personal->update()) {
    echo json_encode(array('mensaje' => 'Personal Modificado con Exito'));
} else {
    echo json_encode(array('mensaje' => 'Personal no pudo ser modificar'));
}

