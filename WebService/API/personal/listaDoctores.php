<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Personal.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$personal = new Personal($db);

//Query
$result = $personal->listaDoctores();

//Get numero de columnas
$num = $result->rowCount();

// Verificar si hay datos
if ($num > 0) {
    // Array
    $personal_arr = array();
    $personal_arr['data'] = array();
    
    while ($row = $result->fetch(PDO::FETCH_NUM)) {
        extract($row);
        $personal_item = array(
            'id_personal' => $row[0],
            'nombre' => $row[1],
            'apellido' => $row[2],
        );
         // Meter la Data
        array_push($personal_arr['data'], $personal_item);
    }
    // Devolver JSON
    echo json_encode($personal_arr);
}else {
    echo json_encode(
        array('message' => 'Personal no encontrados')
    );
}

