<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Personal.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$personal = new Personal($db);

// Get ID
$personal->id = isset($_GET['id']) ? $_GET['id'] : die();

//LLamamos al metodo
$personal->read_single();

// Crear Array
$personal_arr = array(
    'id_personal' => $personal->id_personal,
    'nombre' => $personal->nombre,
    'apellido' => $personal->apellido,
    'dui' => $personal->dui,
    'correo' => $personal->correo,
    'telefono' => $personal->telefono
);

// Creacion de JSON
print_r(json_encode($personal_arr));