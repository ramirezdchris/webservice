<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Personal.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$personal = new Personal($db);

//Enviamos Parametros
$personal->id = isset($_GET['id']) ? $_GET['id'] : die();

$result = $personal->listaDoctorCitas();

$num = $result->rowCount();

if($num > 0){
    $personal_arr = array();
    $personal_arr['data'] = array();
    
    while($row = $result->fetch(PDO::FETCH_NUM)){
        extract($row);
        $cita_item = array(
            'id_cita' => $row[0],  
            'estado' => $row[1],
            'id_expediente' => $row[2],
            'paciente_nombre' => $row[3],
            'paciente_apellido' => $row[4],
            'paciente_correo' => $row[5],
            'paciente_telefono' => $row[6],
            'Estado_cita' => $row[7],    
            
        );
        array_push($personal_arr['data'], $cita_item);
    }
    echo json_encode($personal_arr);
}else{
    echo json_encode(
            array('mensaje' => 'Cita no registradas')
    );
}

