<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Personal.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$personal = new Personal($db);


// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

$personal->nombre = $data->nombre;
$personal->apellido = $data->apellido;
$personal->dui = $data->dui;
$personal->correo = $data->correo;
$personal->telefono = $data->telefono;

// Crear paciente
if ($personal->create()) {
    echo json_encode(array('message' => 'Personal Creado con Exito'));
} else {
    echo json_encode(array('message' => 'Personal no pudo ser creado'));
}

