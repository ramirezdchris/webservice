<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Expediente.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$expediente = new Expediente($db);
$paciente = new Paciente($db);

$result = $expediente->listaPacientes();
$num = $result->rowCount();

// Verificar si hay datos
if ($num > 0 ) {
    $consulta_arr = array();
    $consulta_arr['data'] = array();
    
    while ($row = $result->fetch(PDO::FETCH_NUM) ) {
        extract($row);
        $consulta_item = array(
            'id_expediente' => $row[0],
            'nombre' => $row[1],
            'apellido' => $row[2],
            'dui' => $row[3],
            'carnet' => $row[4],
            'telefono' => $row[5],
            'fecha_nacimiento' => $row[6],
            'correo' => $row[7],
            'pass' => $row[8],
            'id_paciente' => $row[9],                
        );
         // Meter la Data
        //print_r($consulta_item);
        array_push($consulta_arr['data'], $consulta_item);
    }
     
     // Devolver JSON
    echo json_encode($consulta_arr);
}else{
    echo json_encode(
        array('message' => 'Datos de consulta no encontrados')
    );
}


