<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Expediente.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$expediente = new Expediente($db);
$paciente = new Paciente($db);

// Get ID
$expediente->id_expediente = isset($_GET['id']) ? $_GET['id'] : die();

//LLamamos al metodo
$expediente->BuscarIDexpediente();

// Crear Array
$expediente_arr['data'] = array();
$expediente_arra = array(
    'id_expediente' => $expediente->id_expediente,
    'fecha_creacion' => $expediente->fecha_creacion,
    'id_paciente' => $expediente->paciente->id_paciente,
    'nombre' => $expediente->paciente->nombre,
    'apellido' =>$expediente->paciente->apellido,
    'dui' => $expediente->paciente->dui,
    'carnet' => $expediente->paciente->carnet
);
array_push($expediente_arr['data'], $expediente_arra);
// Creacion de JSON
print_r(json_encode($expediente_arr));

