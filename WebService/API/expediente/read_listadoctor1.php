<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Expediente.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$expediente = new Expediente($db);
$paciente = new Paciente($db);

$expediente->id = isset($_GET['id']) ? $_GET['id'] : die();

$result = $expediente->read_doctor1();
$num = $result->rowCount();

// Verificar si hay datos
if ($num > 0 ) {
    $consulta_arr = array();
    $consulta_arr['data'] = array();
    
    while ($row = $result->fetch(PDO::FETCH_ASSOC) ) {
        extract($row);
        $consulta_item = array(
            'Expediente' => $id_expediente,
            'nombre' => $nombre,
            'apellido' => $apellido,
            'telefono' => $telefono,
            'Correo' => $correo,
            //'PErsonal' => $id_personal
        );
         // Meter la Data
        array_push($consulta_arr['data'], $consulta_item);
    }
     
     // Devolver JSON
    echo json_encode($consulta_arr);
}else{
    echo json_encode(
        array('message' => 'Datos de consulta no encontrados')
    );
}


