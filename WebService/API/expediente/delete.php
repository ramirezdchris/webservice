<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/database.php';
include_once '../../models/Expediente.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$expediente = new Expediente($db);

// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

//Obtener el ID para modificar el archivo
$expediente->id_expediente = $data->id_expediente;


// Crear paciente
if ($expediente->delete()) {
    echo json_encode(array('message' => 'Expediente Eliminada con Exito'));
} else {
    echo json_encode(array('message' => 'No pudo ser Eliminado el Expediente'));
}

