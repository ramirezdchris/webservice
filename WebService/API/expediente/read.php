<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Expediente.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$expediente = new Expediente($db);
$paciente = new Paciente($db);

//Query
$result = $expediente->read();

//Get numero de columnas
$num = $result->rowCount();

// Verificar si hay datos
if ($num > 0) {
    // Array
    $expediente_arr = array();
    $expediente_arr['data'] = array();
    
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        $expediente_item = array(
            'id_expediente' => $id_expediente,
            'fecha_creacion' => $fecha_creacion,
            'id_paciente' => $id_paciente,
            'nombre' => $nombre,
            'apellido' => $apellido,
            'dui' => $dui,
            'carnet' => $carnet
            
        );
         // Meter la Data
        array_push($expediente_arr['data'], $expediente_item);
    }
    // Devolver JSON
    echo json_encode($expediente_arr);
}else {
    echo json_encode(
        array('message' => 'No Hay Datos de Expedientes')
    );
}

