<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/database.php';
include_once '../../models/Expediente.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$expediente = new Expediente($db);
$paciente = new Paciente($db);

// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

$expediente->id_expediente = $data->id_expediente;
$expediente->fecha_creacion = $data->fecha_creacion;
$paciente->id_paciente = $data->id_paciente;
$expediente->setPaciente($paciente);

// Crear paciente
if ($expediente->create()) {
    echo json_encode(array('message' => 'Expediente Creado con Exito'));
} else {
    echo json_encode(array('message' => 'No pudo ser creado el Expediente'));
}

