<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$paciente = new Paciente($db);


// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

$paciente->nombre = $data->nombre;
$paciente->apellido = $data->apellido;
$paciente->dui = $data->dui;
$paciente->carnet = $data->carnet;
$paciente->correo = $data->correo;
$paciente->telefono = $data->telefono;
$paciente->fecha_nacimiento = $data->fecha_nacimiento;
$paciente->pass = $data->pass;

// Crear paciente
if ($paciente->create()) {
    echo json_encode(array('message' => 'Paciente Creado'));
} else {
    echo json_encode(array('message' => 'Paciente no pudo ser creado'));
}