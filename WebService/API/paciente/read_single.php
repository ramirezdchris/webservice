<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$paciente = new Paciente($db);

// Get ID
$paciente->id = isset($_GET['id']) ? $_GET['id'] : die();

// Get Post
$paciente->read_single();

// Crear Array
$paciente_arr = array(
    'id_paciente' => $paciente->id_paciente,
    'nombre' => $paciente->nombre,
    'apellido' => $paciente->apellido,
    'dui' => $paciente->dui,
    'correo' => $paciente->correo,
    'telefono' => $paciente->telefono,
    'fecha_nacimiento' => $paciente->fecha_nacimiento
);

// Creacion de JSON
print_r(json_encode($paciente_arr));