<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$paciente = new Paciente($db);


// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

// Obtener el ID para actualizar
$paciente->id_paciente = $data->id_paciente;

$paciente->nombre = $data->nombre;
$paciente->apellido = $data->apellido;
$paciente->dui = $data->dui;
$paciente->carnet = $data->carnet;
$paciente->telefono = $data->telefono;
$paciente->fecha_nacimiento = $data->fecha_nacimiento;
$paciente->correo = $data->correo;
$paciente->pass = $data->pass;
$paciente->id_paciente = $data->id_paciente;
// Crear paciente
if ($paciente->update()) {
    echo json_encode(array('message' => 'Paciente fue actualizado'));
} else {
    echo json_encode(array('message' => 'Paciente no pudo ser actualizado'));
}