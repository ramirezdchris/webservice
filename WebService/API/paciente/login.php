<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Paciente.php';


// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$paciente = new Paciente($db);

$paciente->correo = isset($_GET['correo']) ? $_GET['correo'] : die();
$paciente->pass = isset($_GET['pass']) ? $_GET['pass'] : die();

$paciente_arr = $paciente->login();

print_r(json_encode($paciente_arr));