<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$paciente = new Paciente($db);


$id_paciente = isset($_GET['id_paciente']) ? $_GET['id_paciente'] : die();

// Obtener datos enviados sin ser procesados
//$data = json_decode(file_get_contents("php://input"));

//var_dump($data);

// Obtener el ID para actualizar

$paciente->id_paciente = $id_paciente;
echo $id_paciente;

// Crear paciente
if ($paciente->delete()) {
    echo json_encode(array('message' => 'Paciente fue eliminado'));
} else {
    echo json_encode(array('message' => 'Paciente no pudo eliminado'));
}