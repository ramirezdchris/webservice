<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Content-Type: application/x-www-form-urlencoded');


include_once '../../config/database.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$paciente = new Paciente($db);

//Query
$result = $paciente->read();

//Get numero de columnas
$num = $result->rowCount();

// Verificar si hay datos
if ($num > 0) {
    // Array
    $paciente_arr = array();
    $paciente_arr['data'] = array();

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        $paciente_item = array(
            'id_paciente' => $id_paciente,
            'nombre' => $nombre,
            'apellido' => $apellido,
            'dui' => $dui,
            'correo' => $correo,
            'telefono' => $telefono,
            'fecha_nacimiento' => $fecha_nacimiento
        );

        // Meter la Data
        array_push($paciente_arr['data'], $paciente_item);
    }
    // Devolver JSON
    echo json_encode($paciente_arr);
} else {
    echo json_encode(
        array('message' => 'Paciente no encontrados')
    );
}