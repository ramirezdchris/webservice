<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Paciente.php';
include_once '../../models/Expediente.php';
include_once '../../models/Cita.php';


// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

$cita = new Cita($db);
$personal = new Personal($db);
$expediente = new Expediente($db);

$data = json_decode(file_get_contents("php://input"));

$cita->dia = $data->dia;
$cita->hora = $data->hora;
$cita->estado = $data->estado;
$personal->id_personal = $data->idp;
$expediente->id_expediente = $data->idex;
$cita->setPersonal($personal);
$cita->setExpediente($expediente);

$resull2 = $cita->consultarCita();

if($resull2){
    echo json_encode(array('mensaje' => 'Ya existe una cita a esa misma hora y fecha'));
}else{
    if($cita->create()){
        echo json_encode(array('mensaje' => 'Cita Creada'));
    }   
    else {
        echo json_encode(array('mensaje' => 'Cita no pudo ser creada'));
    }
}

        
