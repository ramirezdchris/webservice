<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once '../../config/database.php';
include_once '../../models/Cita.php';

$database = new DataBase();
$db = $database->connect();

$cita = new Cita($db);

$result = $cita->listaCitasHistorial();

$num = $result->rowCount();

if($num > 0){
    $cita_arr = array();
    $cita_arr['data'] = array();
    
    while($row = $result->fetch(PDO::FETCH_NUM)){
        extract($row);
        $cita_item = array(
            'id_cita' => $row[0],  
            'estado' => $row[1],
            'id_expediente' => $row[2],
            'paciente_nombre' => $row[3],
            'paciente_apellido' => $row[4],
            'paciente_correo' => $row[5],
            'paciente_telefono' => $row[6]
            
        );
        array_push($cita_arr['data'], $cita_item);
    }
    echo json_encode($cita_arr);
}else{
    echo json_encode(
            array('mensaje' => 'Cita no registradas')
    );
}

