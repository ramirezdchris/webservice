<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Cita.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$cita = new Cita($db);


// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

//Obtener el ID para ELIMINAR el archivo
$cita->id_cita = $data->id_cita;


// Crear paciente
if ($cita->delete()) {
    echo json_encode(array('mensaje' => 'Cita fue elimina con Exito'));
} else {
    echo json_encode(array('mensaje' => 'Cita no pudo ser eliminada'));
}
