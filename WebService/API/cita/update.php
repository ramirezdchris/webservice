<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Cita.php';
include_once '../../models/Paciente.php';
include_once '../../models/Personal.php';
include_once '../../models/Paciente.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();

// Pasando parametros de conexion
$cita = new Cita($db);
$paciente = new Paciente($db);
$personal = new Personal($db);
$expediente = new Expediente($db);

// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

$cita->id_cita = $data->id_cita;
$cita->hora = $data->hora;
$cita->dia = $data->dia;
$personal->id_personal = $data->id_personal;
$expediente->id_expediente = $data->id_expediente;
$cita->setExpediente($expediente);
$cita->setPersonal($personal);


$resull2 = $cita->consultarCita();

if($resull2){
    echo json_encode(array('mensaje' => 'Ya existe una cita a esa misma hora y fecha'));
}else{
    if($cita->update()){
        echo json_encode(array('mensaje' => 'Cita Actualizada'));
    }   
    else {
        echo json_encode(array('mensaje' => 'Cita no fue '));
    }
}

