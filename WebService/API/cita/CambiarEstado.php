<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');


include_once '../../config/database.php';
include_once '../../models/Cita.php';

// Instaciar la Clase DataBase & connect
$database = new DataBase();
$db = $database->connect();
$this->conn = $db;

// Pasando parametros de conexion
$cita = new Cita($db);


// Obtener datos enviados sin ser procesados
$data = json_decode(file_get_contents("php://input"));

//Obtener el ID para modificar el archivo
$cita->id_cita = $data->id_cita;
$cita->estado = $data->estado;

// Crear paciente
if ($cita->CambiarEstado()) {
    echo json_encode(array('message' => 'Estado de Cita Modificado con Exito'));
} else {
    echo json_encode(array('message' => 'Estado de Cita no pudo ser modificado'));
}

