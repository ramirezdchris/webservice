<?php
/**
 * This example shows settings to use when sending via Google's Gmail servers.
 * This uses traditional id & password authentication - look at the gmail_xoauth.phps
 * example to see how to use XOAUTH2.
 * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
 */

//Import PHPMailer classes into the global namespace
//use PHPMailer\PhpMailer\PHPMailer;
//use PHPMailer\PhpMailer\SMTP;
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require './PHPMailer/PHPMailer.php';
require './PHPMailer/SMTP.php';
require './PHPMailer/Exception.php';

//include_once '../../config/database.php';
include_once(__DIR__.'/../config/database.php');

$conn;

$database = new DataBase();
$db = $database->connect();
$conn = $db;

$data = json_decode(file_get_contents("php://input"));
$user = $data->user;
//$pass = $data->pass;

//Create a new PHPMailer instance
$mail = new PHPMailer;

$correoEnviar = "ramirezd.christian@gmail.com";
$nuevaContraseña = "123";
//Tell PHPMailer to use SMTP
$mail->isSMTP();

//Enable SMTP debugging
// SMTP::DEBUG_OFF = off (for production use)
// SMTP::DEBUG_CLIENT = client messages
// SMTP::DEBUG_SERVER = client and server messages
//$mail->SMTPDebug = SMTP::DEBUG_SERVER;

//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

//Set the encryption mechanism to use - STARTTLS or SMTPS
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = 'clinicautec2020@gmail.com';

//Password to use for SMTP authentication
$mail->Password = 'clinicautec';

//Set who the message is to be sent from
$mail->setFrom('clinicautec2020@gmail.com', 'Clinica UTEC');

//Set an alternative reply-to address
//$mail->addReplyTo('replyto@example.com', 'First Last');

//Set who the message is to be sent to
$mail->addAddress($user);

        $query = 'SELECT p.id_personal, p.nombre, p.apellido, id_rol, p.correo FROM clinicautec.usuario'  . ' user INNER JOIN ' . 'clinicautec.personal p ON p.id_personal = user.id_personal' . ' WHERE p.correo = ?';
        
        $query2 = 'SELECT p.correo FROM clinicautec.paciente p WHERE' .' p.correo = ?';
        
        $stmt = $conn->prepare($query);
        $stmt->bindParam(1, $user, PDO::PARAM_STR);       
        $stmt->execute();
        
        
        $stmt2 = $conn->prepare($query2);
        $stmt2->bindParam(1, $user, PDO::PARAM_STR);        
        $stmt2->execute();
        
        $row = $stmt->fetch(PDO::FETCH_ASSOC);         
        $row2 = $stmt2->fetch(PDO::FETCH_ASSOC);      
        
        if(($row == null) && ($row2 == null)){           
            //$crede = array('id_usuario' => 0,'nombre' => 0, 'apellido' =>0 , 'rol' => 0);
            //echo json_encode(array('mensaje' => 'Este correo no existe en nuestra base de datos'));
            //$mail->clearAddresses();
        }else if($row2 != null){
            //$crede = array('id_usuario' => $this->expediente->id_expediente, 'nombre' => $this->paciente->nombre, 'apellido' => $this->paciente->apellido, 'rol' => '4');
            $caracteres = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            for($x = 0; $x < 10; $x++){
                $aleatoria = substr(str_shuffle($caracteres), 0, 5);
                //echo $aleatoria . "\n";               
            }
            $correo = $row2['correo'];
            $update = "UPDATE clinicautec.paciente SET pass =  '$aleatoria'" . "WHERE correo = '$correo';";
            //echo $update;
            $s = $conn->prepare($update);
            $s->execute();
            echo json_encode(array('mensaje' => 'Su nueva clave a sido enviada a su correo electronico'));
            //Set the subject line
            $mail->Subject = 'Recuperar Clave';

            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            //$mail->msgHTML(file_get_contents('contents.html'), __DIR__);

            $mail->Body = "Su nueva contraseña sera: " .$aleatoria;
            
            
        }else if($row != null){
            //$crede = array('id_usuario' => $this->personal->id_personal, 'nombre' => $this->personal->nombre, 'apellido' => $this->personal->apellido, 'rol' => $this->rol->id_rol);          
            $caracteres = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            for($x = 0; $x < 10; $x++){
                $aleatoria = substr(str_shuffle($caracteres), 0, 5);
                //echo $aleatoria . "\n";                
            }
            $update = "UPDATE clinicautec.usuario u INNER JOIN clinicautec.personal p ON p.id_personal = u.id_personal  SET u.pass = '$aleatoria'" ." WHERE p.id_personal = ".$row['id_personal'];
            //echo $update;
            $s = $conn->prepare($update);                
            $s->execute();
            echo json_encode(array('mensaje' => 'Su nueva clave a sido enviada a su correo electronico'));
            $mail->Subject = 'Recuperar Clave';

            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            //$mail->msgHTML(file_get_contents('contents.html'), __DIR__);

            $mail->Body = "Su nueva contraseña sera: " .$aleatoria;
        }



//Replace the plain text body with one created manually

$mail->AltBody = 'Codigo de tu contraseña';

//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
    //echo 'Mailer Error: '. $mail->ErrorInfo;
    echo json_encode(array('mensaje' => 'Este usuario no existe en nuestra base de datos'));
} else {    
        
        
    
    //Section 2: IMAP
    //Uncomment these to save your message in the 'Sent Mail' folder.
    #if (save_mail($mail)) {
    #    echo "Message saved!";
    #}
}

?>

